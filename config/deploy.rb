# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "aug"
set :user,  ENV["user"] || ENV["USER"] || "smartmobe"

set :scm, nil

# Multistage Deployment #
#####################################################################################
set :stages,              %w(dev staging prod)
set :default_stage,       "dev"


# Other Options #
#####################################################################################
set :ssh_options,         { :forward_agent => true }
set :default_run_options, { :pty => true }


# Permissions #
#####################################################################################
set :use_sudo,            true
set :permission_method,   :acl
set :use_set_permissions, true
set :webserver_user,      "deploy"
set :group,               "www-data"
set :keep_releases,       1


# Set current time #
#######################################################################################
require 'date'
set :current_time, DateTime.now
set :current_timestamp, DateTime.now.to_time.to_i


# Setup Tasks #
#######################################################################################
namespace :setup do
    desc "Create overlay folders"
    task :create_overlay_folder do
        on roles(:all) do
            execute "mkdir -p #{fetch(:overlay_path)}"
        end
    end

    desc "Create temp folder"
    task :create_temp_folder do
        on roles(:all) do
            execute "mkdir -p #{fetch(:tmp_dir)}"
        end
    end
end

# Installation Tasks #
#######################################################################################
namespace :installation do

    desc "Create ver.txt"
    task :create_ver_txt do
        on roles(:all) do
            puts ("--> Copying ver.txt file")
            execute "cp #{release_path}/config/deploy/ver.txt.example #{release_path}/public/ver.txt"
            execute "sed --in-place 's/%date%/#{fetch(:current_time)}/g
                        s/%branch%/#{fetch(:branch)}/g
                        s/%revision%/#{fetch(:current_revision)}/g
                        s/%deployed_by%/#{fetch(:user)}/g' #{release_path}/public/ver.txt"
            execute "find #{release_path}/public -type f -name 'ver.txt' -exec chmod 664 {} \\;"
        end
    end
end

# Tasks Execution #
#######################################################################################
desc "Copy dist file to server and extract"
task :upload_dist_file do
    on roles(:all) do
        execute "mkdir -p #{releases_path}/#{now}"
        upload! "build/dist.zip", fetch(:tmp_dir)
        execute "rm -rf #{fetch(:application)}/temp/#{fetch(:application)}"
        execute "unzip /home/deploy/#{fetch(:application)}/temp/dist.zip -d #{fetch(:application)}/temp/#{fetch(:application)}"
    end
end

desc "Move files to release folder"
task :move_files do
    on roles(:all) do 
        within fetch(:tmp_dir) do
            execute "cp -a #{fetch(:tmp_dir)}/#{fetch(:application)}/. #{releases_path}/#{now}/"
        end
    end
end

desc "Setup Initialize"
task :setup_init do
    # invoke "setup:init"
    invoke "setup:create_temp_folder"
end


desc "Deploy Application"
namespace :deploy do
    after :started, "upload_dist_file"
    after :started, "move_files"
end




